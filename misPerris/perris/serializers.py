from .models import perro
from rest_framework import serializers

class PerrosSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = perro
        fields = ('foto','nombre','raza','descripcion','estado')