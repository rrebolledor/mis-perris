from django.shortcuts import render
from django.template import RequestContext
from django.contrib.auth.forms import AuthenticationForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth import login,authenticate
from .forms import PerfilForm,UserForm,LoginForm,perroForm
from django.contrib.auth.models import User
from django.views.generic.edit import FormView,DeleteView,UpdateView
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from django.contrib.auth.hashers import *
from django.contrib.auth.models import Group
from django.core.cache import cache
from django.contrib import messages
from django.contrib.auth.decorators import login_required,user_passes_test
from django.utils.decorators import method_decorator
from django.contrib.auth.views import LoginView,LogoutView
from .models import perro
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import viewsets
from .serializers import PerrosSerializer

# Create your views here.
grupo = None
def Home(request):
    return render(request,'perris/index.html',{})
@login_required(login_url='login.html')
def Home2(request):
    return render(request,'perris/index2.html',{})

def base(request):
    return render(request,'perris/base.html')


def errorPermisos(request):
    return render(request,'perris/errorpermisos.html')

#VISTA PARA FORMULARIO CONTACTO
def Contacto(request):
    if request.method == "POST":
        profileForm = PerfilForm(request.POST)
        usuarioForm = UserForm(request.POST)

        if profileForm.is_valid() and usuarioForm.is_valid():
            user = usuarioForm.save(commit=False)
            clave_virgen = usuarioForm.cleaned_data['password']
            clave_encriptada = make_password(clave_virgen,None,'md5')
            user.set_password(clave_encriptada)
            user.save() 
            user.groups.add(Group.objects.get(name='usuario_promedio'))
            instancia = profileForm.save(commit=False)
            instancia.user_id = User.objects.latest('id').id
            instancia.save()
           
        else:
            print(profileForm.errors)
            print(usuarioForm.errors)
    else:
        profileForm = PerfilForm()
        usuarioForm = UserForm()
    return render(request,'perris/form.html',{'form': profileForm})


class Login(LoginView):
        template_name = 'perris/login.html'



def superuser_or_moderador(user):
    return user.is_superuser or user.groups.filter(name='moderador').exists()



@login_required(login_url='login.html')
@user_passes_test(superuser_or_moderador,login_url='/errorpermisos')
def subir(request):
    if request.method == "POST":
        print("post")
        form = perroForm(request.POST,request.FILES,request.user)
        print(form.errors)
        if form.is_valid():
            print("valido")
            form.save()
    return render(request,'perris/upload.html',{'form': perroForm})

@login_required(login_url='login.html')
def menu(request):
    if request.user.groups.filter(name='moderador').exists():
        return render(request,'perris/menuModerador.html',{})
    else:
        return render(request,'perris/menuNormal.html',{})

@login_required(login_url='login.html')
def listaPerro(request):
    todos = perro.objects.all()
    disponibles = perro.objects.filter(estado = 'D')
    return render(request,'perris/perro_list.html',{'todos':todos,'disponibles':disponibles})




class detallesPerro(DetailView):
    model = perro
    @method_decorator(login_required(login_url='/login.html'))
    def dispatch(self, *args, **kwargs):
        return super(detallesPerro, self).dispatch(*args, **kwargs)


class editarPerro(UpdateView,LoginRequiredMixin):
    model = perro
    success_url = reverse_lazy('perris:list')
    fields = ('foto','nombre','raza','descripcion','estado')
    login_url = 'login.html'
    @method_decorator(login_required(login_url='/login.html'))
    @method_decorator(user_passes_test(superuser_or_moderador,login_url='/errorpermisos'))
    def dispatch(self, *args, **kwargs):
        return super(editarPerro, self).dispatch(*args, **kwargs)


class borrarPerro(DeleteView,LoginRequiredMixin):
    model = perro
    success_url = reverse_lazy('perris:list')
    login_url = 'login.html'
    @method_decorator(login_required(login_url='/login.html'))
    @method_decorator(user_passes_test(superuser_or_moderador,login_url='/errorpermisos'))
    def dispatch(self, *args, **kwargs):
        return super(borrarPerro, self).dispatch(*args, **kwargs)


class SignOutView(LogoutView):
    pass



class PerroViewSet(viewsets.ModelViewSet):
    queryset = perro.objects.filter(estado = 'D')
    serializer_class = PerrosSerializer
    

