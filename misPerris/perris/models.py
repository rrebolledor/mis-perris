from django.db import models
from django import forms
from django.contrib.auth.models import User
# Create your models here.


class usuario (models.Model):
    user = models.OneToOneField(User,on_delete = "models.CASCADE")
    run = models.CharField( primary_key = True, max_length = 10)
    nombre = models.CharField(max_length = 150)
    fNacimiento = models.DateField()
    fono = models.IntegerField()
    region = models.CharField(max_length = 50)
    ciudad = models.CharField(max_length = 50)
    tipoVivienda = models.CharField(max_length = 100)

    def __str__(self):
        return self.user.username + ' ~ ' + self.run
    


class perro(models.Model):
    foto = models.ImageField(upload_to="perros", height_field=None, width_field=None, max_length=None)
    nombre = models.CharField(max_length = 150)
    raza = models.CharField(max_length = 150)
    descripcion = models.CharField(max_length = 300)
    estados = (
        ('R','Rescatado'),
        ('D','Disponible'),
        ('A','Adoptado')
    )
    estado = models.CharField(max_length=1,choices = estados)

    def __str__(self):
        return self.nombre +' ~ '+ self.raza + ' ~ ' + self.estado
    


    