# Generated by Django 2.1.2 on 2018-10-19 14:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('perris', '0002_auto_20181019_1141'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='adopcion',
            name='id',
        ),
        migrations.AlterField(
            model_name='adopcion',
            name='run',
            field=models.CharField(max_length=10, primary_key=True, serialize=False),
        ),
    ]
