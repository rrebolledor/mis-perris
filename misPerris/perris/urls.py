from django.conf.urls import include,url
from . import views
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers

app_name = 'perris'


router = routers.DefaultRouter()
router.register(r'perro',views.PerroViewSet)

urlpatterns = [
    url(r'^$', views.Home),
    url(r'^in', views.Home2),
    url(r'^registro',views.Contacto),
    url(r'^login',views.Login.as_view()),
    url(r'^cerrar-sesion/$',views.SignOutView.as_view(), name='cerrar_sesion'),
    url(r'^errorpermisos',views.errorPermisos),
    url(r'^servicios',views.menu),
    url(r'^subir',views.subir),
    url(r'^perros', views.listaPerro, name='list'),
    url(r'^(?P<pk>\d+)$', views.detallesPerro.as_view(), name='detail'),
    url(r'^editar/(?P<pk>\d+)$', views.editarPerro.as_view(), name='edit'),
    url(r'^borrar/(?P<pk>\d+)$', views.borrarPerro.as_view(), name='delete'),
    url(r'^api/',include(router.urls)),
    url(r'base',views.base)
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)