from django import forms
from .models import usuario,perro
from django.contrib.auth.models import User

class PerfilForm(forms.ModelForm):

    class Meta:
        model = usuario
        fields = ('run','nombre','fNacimiento','fono','region','ciudad','tipoVivienda')

class perroForm(forms.ModelForm):
    class Meta:
        model = perro
        fields = ('foto','nombre','raza','descripcion','estado')



class UserForm(forms.ModelForm):
    
    class Meta:
        model = User
        fields = ('username','password','email')




class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget = forms.PasswordInput())